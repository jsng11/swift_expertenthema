//
//  AnimationChoices.swift
//  Expertenthema
//
//  Created by Jonathan Schoengarth on 23.05.22.
//

import SwiftUI

struct AnimationChoices: View {
    @State private var showText1 = false
    @State private var showText2 = false
    @State private var showText3 = false
    var body: some View {
        VStack{
           Spacer()
            HStack{
                Text("Implicit animation")
                Spacer()
                Button("Click") {
                    showText1.toggle()
                }
                .padding(10)
                .background(.blue)
                .foregroundColor(.white)
                .cornerRadius(16)
            }
            Divider()
            HStack{
                Text("Explicit animation")
                Spacer()
                Button("Click") {
                    withAnimation(.linear(duration: 1)) {
                        showText2.toggle()
                    }
                }
                .padding(10)
                .background(.red)
                .foregroundColor(.white)
                .cornerRadius(16)
            }
            Divider()
            Toggle("Binding animation", isOn: $showText3.animation(.linear(duration: 1)))
                .tint(.orange)
            Spacer()
            
                Text("Implicit animation")
                    .frame(maxWidth: .infinity)
                    .padding()
                    .background(.blue)
                    .foregroundColor(.white)
                    .cornerRadius(8)
                    .opacity(showText1 ? 1 : 0)
                    .animation(.linear(duration:1), value: showText1)
            
                Text("Explicit animation")
                    .frame(maxWidth: .infinity)
                    .padding()
                    .background(.red)
                    .foregroundColor(.white)
                    .cornerRadius(8)
                    .opacity(showText2 ? 1 : 0)

                Text("Binding animation")
                    .frame(maxWidth: .infinity)
                    .padding()
                    .background(.orange)
                    .foregroundColor(.white)
                    .cornerRadius(8)
                    .opacity(showText3 ? 1 : 0)
        }.padding()
    }
}

struct AnimationChoices_Previews: PreviewProvider {
    static var previews: some View {
        AnimationChoices()
    }
}
