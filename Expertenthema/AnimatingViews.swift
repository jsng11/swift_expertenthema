//
//  AnimationChoices.swift
//  Expertenthema
//
//  Created by Marc Troell on 29.05.22.
//

import SwiftUI

struct AnimatingViews: View {
    @State private var showDetail = false

    var body: some View {
        VStack {
            Text("example view (team members)")
            HStack {
                Text("Gruppe F:")
                    .font(.largeTitle)
                    

                Button {
                    withAnimation(.easeInOut(duration: 2)) {
                        showDetail.toggle()
                    }
                } label: {
                    Label("Teammitglieder", systemImage: "chevron.right.circle")
                        .labelStyle(.iconOnly)
                        .imageScale(.large)
                        .rotationEffect(.degrees(showDetail ? 90 : 0))
                        .scaleEffect(showDetail ? 1.5 : 1)
                        .padding()
                }
            }

            if showDetail {
                Teammembers()
            }
        }
    }
}

struct AnimatingViews_Previews: PreviewProvider {
    static var previews: some View {
        VStack {
            Spacer()
        }
    }
}
