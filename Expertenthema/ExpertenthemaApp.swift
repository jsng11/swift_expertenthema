//
//  ExpertenthemaApp.swift
//  Expertenthema
//
//  Created by Jonathan Schoengarth on 23.05.22.
//

import SwiftUI

@main
struct ExpertenthemaApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
