//
//  ContentView.swift
//  Expertenthema
//
//  Created by Jonathan Schoengarth on 23.05.22.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        NavigationView{
            VStack(spacing: 200) {
                NavigationLink(destination: AnimationChoices()){
                    Text("AnimationChoices")
                }
                NavigationLink(destination: AnimatingViews()){
                    Text("Auf- und Zuklappen von Views")
                }
            }
        }
    }
}



struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
