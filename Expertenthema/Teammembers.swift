//
//  Test.swift
//  Expertenthema
//
//  Created by Marc on 29.05.22.
//

import SwiftUI

struct Teammembers: View {
    var body: some View {
        Text("Marco Grabiec")
        Text("Jonathan Schöngarth")
        Text("Marc Tröll")
        Text("Yousif Wardah")
    }
}

struct Teammembers_Previews: PreviewProvider {
    static var previews: some View {
        Teammembers()
    }
}
